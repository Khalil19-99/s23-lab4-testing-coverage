import java.sql.Timestamp;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;

// full basis path coverage for function setPost
class postDAOTests {
    JdbcTemplate jdbcMock;
    PostDAO postDAO;
    Post firstPost;
    static String firstAuthor = "author";
    static String firstMessage = "message";
    static Timestamp firstCreated = new Timestamp(0);
    static String secondAuthor = "author2";
    static String secondMessage = "message2";
    static Timestamp secondCreated = new Timestamp(1);

    @BeforeEach
    void init() {
        firstPost = makePost(firstAuthor, firstMessage, firstCreated);
        jdbcMock = Mockito.mock(JdbcTemplate.class);
        postDAO = new PostDAO(jdbcMock);
        Mockito.when(
                jdbcMock.queryForObject(
                        Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.eq(0)))
                .thenReturn(firstPost);
    }

    private static Post makePost(String author, String message, Timestamp ts) {
        Post post = new Post();
        post.setAuthor(author);
        post.setMessage(message);
        post.setCreated(ts);
        return post;
    }

    private static Stream<Arguments> provideArgsWithUpdate() {
        return Stream.of(
                Arguments.of(0, makePost(firstAuthor, firstMessage, secondCreated),
                        "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(0, makePost(firstAuthor, secondMessage, firstCreated),
                        "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Arguments.of(0, makePost(firstAuthor, secondMessage, secondCreated),
                        "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(0, makePost(secondAuthor, firstMessage, firstCreated),
                        "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Arguments.of(0, makePost(secondAuthor, firstMessage, secondCreated),
                        "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(0, makePost(secondAuthor, secondMessage, firstCreated),
                        "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Arguments.of(0, makePost(secondAuthor, secondMessage, secondCreated),
                        "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"));
    }

    @ParameterizedTest
    @MethodSource("provideArgsWithUpdate")
    void TestSetPostWithUpdate(Integer id, Post post, String expected) {
        PostDAO.setPost(id, post);
        Mockito.verify(jdbcMock).update(Mockito.eq(expected), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void TestSetPostWithoutUpdate() {
        PostDAO.setPost(0, firstPost);
        Mockito.verify(jdbcMock, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.any()));
    }
}
